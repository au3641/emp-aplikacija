package spektrogram.emp.spektrogram_emp;


// Calculate Hann Window for a given size (used with fft)
public class HannWindow {
    public static double[] window;

    public static void loadHannWindowFunction(int size)
    {
        window = new double[size];
        for (int n = 0; n < size; n++)
        {
            window[n] = Math.pow(Math.sin((Math.PI*(double)n) / (double)(size-1)),2);
            //output[n] = sin ^ 2 ((pi * n) / input.length - 1);
        }
    }
}

// to make spectrogram:
// - make hann windows function array
// - take sample with size 2^n
// - multiply sample[i] with hannarray[i]
// - throw sample in fft
// - take result, throw second half away
// - take result, throw array in complex.abs();