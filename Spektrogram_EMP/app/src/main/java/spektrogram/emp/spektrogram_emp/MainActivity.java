package spektrogram.emp.spektrogram_emp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.EditText;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.appindexing.Action;
//import com.google.android.gms.appindexing.AppIndex;
//import com.google.android.gms.appindexing.Thing;
//import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import omrecorder.AudioChunk;
import omrecorder.AudioSource;
import omrecorder.OmRecorder;
import omrecorder.PullTransport;
import omrecorder.Recorder;

public class MainActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    //    private String mTempFilename;
    private MediaRecorder mRecorder = null;
    private Recorder mOmRecorder;
    private MediaPlayer mPlayer;
    private boolean mPlaying = false;
    private boolean mRecording = false;
    private boolean mRecordingPaused = false;
    private File mFile;

    private static int display_color = 0;
    private static int BUFFER_SIZE = 2048;
    private static final int SMALLBUF_SIZE = 512;
    private static int RECORDER_SAMPLERATE = 22050;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
//   private GoogleApiClient client;
    private boolean bitmapResizeu = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        HannWindow.loadHannWindowFunction(SMALLBUF_SIZE);
        display = Bitmap.createBitmap(512, 256, Bitmap.Config.ARGB_8888);
        display.eraseColor(Color.BLACK);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        if(intent != null){
            String filename = intent.getStringExtra("spektrogram.emp.filename");
            if(filename != null){
                mFile =  new File(filename);
                if(mFile != null){
                    TextView tv = (TextView)findViewById(R.id.filenameText);
                    tv.setText(mFile.getName());
                }
            }
        }

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        //SharedPreferences prefs = this.getSharedPreferences("pref_general",0);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        RECORDER_SAMPLERATE = Integer.parseInt(prefs.getString("list_preference_frequency_range", "22050"));
        display_color = Integer.parseInt(prefs.getString("list_preference_color", "3"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.action_saved: {
                stopRecordingWav(false);
                Intent intent = new Intent(MainActivity.this,
                        SavedFilesActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.action_settings: {
                stopRecordingWav(false);
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.EXTRA_SHOW_FRAGMENT, SettingsActivity.GeneralPreferenceFragment.class.getName());
                intent.putExtra(SettingsActivity.EXTRA_NO_HEADERS, true);
                startActivity(intent);
                break;
            }
        }

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    int mPlayerPosition = 0;
    public void OnPlayClicked(View view) {

        FloatingActionButton fab = (FloatingActionButton) view;

        if(mRecording && !mPlaying)
        {
            if(mRecordingPaused)
            {
                resumeRecordingWav();
                fab.setImageResource(R.drawable.ic_action_pause);
            }
            else
            {
                pauseRecordingWav();
                fab.setImageResource(R.drawable.ic_action_play);
            }
        }
        else if (mPlaying) {
            fab.setImageResource(R.drawable.ic_action_play);
            if(mPlayer != null){
                mPlayer.pause();
                mPlayerPosition=mPlayer.getCurrentPosition();
            }
            mPlaying = !mPlaying;
        } else {
            if(mFile == null){
                Toast.makeText(this, R.string.toast_no_file_selected, Toast.LENGTH_SHORT).show();
                return;
            }
            fab.setImageResource(R.drawable.ic_action_pause);
            mPlaying = !mPlaying;
            if(mPlayer != null){
                mPlayer.seekTo(mPlayerPosition);
                mPlayer.start();
            }
            else {
                play();
            }
        }
    }

    private void play(){
        try {
            mPlayer = new MediaPlayer();
            mPlayer.setDataSource(mFile.getPath());
            mPlayer.prepare();
            mPlayer.start();
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    // TODO Auto-generated method stub
                    mp.release();
                    mPlayer = null;
                    ((TextView)findViewById(R.id.filenameText)).setText("");
                }

            });
            mPlayer.setLooping(false);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void OnRecordClicked(View view) {
        FloatingActionButton fab = (FloatingActionButton) view;
        FloatingActionButton fab_play = (FloatingActionButton)findViewById(R.id.fab_Play);
        if (mRecording) {
            fab.setImageResource(R.drawable.ic_action_record);
            fab_play.setImageResource(R.drawable.ic_action_play);
            stopRecordingWav(true);
        } else {
            fab.setImageResource(R.drawable.ic_action_stop);
            fab_play.setImageResource(R.drawable.ic_action_pause);
            startRecordingWav();
        }
        mRecording = !mRecording;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        RECORDER_SAMPLERATE = Integer.parseInt(prefs.getString("list_preference_frequency_range", "0"));
        display_color = Integer.parseInt(prefs.getString("list_preference_color", "3"));
    }

    private void ShowSaveDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Vnesi naslov posnetka:");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("Shrani", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                File inFile = tempWavFile(); // new File(mTempFilename);
                if (!inFile.exists()) return;
                File outFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Lib/" + input.getText().toString() + ".wav");
                try {
                    File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Lib");
                    if (!folder.exists()) folder.mkdir();
                    outFile.createNewFile();
                    copy(inFile, outFile);
                    inFile.delete();
                } catch (IOException e) {
                    e.printStackTrace();
                    inFile.delete();
                }
            }
        });
        builder.setNegativeButton("Prekliči", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


    private void copy(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

    private void startRecordingWav() {
        mOmRecorder = OmRecorder.wav(
                new PullTransport.Default(mic(), new PullTransport.OnAudioChunkPulledListener() {
                    @Override
                    public void onAudioChunkPulled(AudioChunk audioChunk) {
                        displayChunk(audioChunk.toShorts());
                    }
                }), tempWavFile());
        mOmRecorder.startRecording();
    }

    private void pauseRecordingWav()
    {
        if(mOmRecorder != null && !mRecordingPaused) {
            mOmRecorder.pauseRecording();
            mRecordingPaused = true;
        }
    }

    private void resumeRecordingWav()
    {
        if(mOmRecorder != null && mRecordingPaused){
            mOmRecorder.resumeRecording();
            mRecordingPaused = false;
        }
    }

    private void stopRecordingWav(boolean save) {
        if(mOmRecorder != null){
            mOmRecorder.stopRecording();
            if(save){
                Intent intent = new Intent(MainActivity.this, GetFilenameHelpActivity.class);
                startActivityForResult(intent, 1234);

             //   ShowSaveDialog();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1234) {
            if (resultCode == RESULT_OK) {
                final String result = data.getStringExtra("spektrogram.emp.returned_filename");
                File inFile = tempWavFile(); // new File(mTempFilename);
                if (!inFile.exists()) return;
                File outFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Lib/" + result + ".wav");
                try {
                    File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Lib");
                    if (!folder.exists()) folder.mkdir();
                    outFile.createNewFile();
                    copy(inFile, outFile);
                    inFile.delete();
                } catch (IOException e) {
                    e.printStackTrace();
                    inFile.delete();
                }
            }
        }
    }

    private File tempWavFile() {
        return new File(Environment.getExternalStorageDirectory(), "spectrogram_4fe438e3-8854-48b3-a1fa-826625098976.wav");
    }

    private AudioSource mic() {
        return new AudioSource.Smart(MediaRecorder.AudioSource.MIC, RECORDER_AUDIO_ENCODING,
                RECORDER_CHANNELS, RECORDER_SAMPLERATE);
    }



// to make spectrogram:
// - make hann windows function array
// - take sample with size 2^n
// - multiply sample[i] with hannarray[i]
// - throw sample in fft
// - take result, throw second half away
// - take result, throw array in complex.abs();
    //private int x = 0, y = 0;
    private int bufferPos = 0;
    private double[][] displayDataBuffer = new double[512][];
    private short[] chunkBuffer = new short[SMALLBUF_SIZE];
    private int chunkBufferPos = 0;
    private boolean useChunkBuffer = false;
    private Paint painter = new Paint();

    Bitmap display;
    private void displayChunk(short[] audioData)
    {
        if(audioData.length < SMALLBUF_SIZE && chunkBufferPos < SMALLBUF_SIZE)
        {
            for(int i = 0; i < audioData.length; i++)
            {
                if(chunkBufferPos >= SMALLBUF_SIZE)
                    break;
                chunkBuffer[chunkBufferPos] = audioData[i];
                chunkBufferPos++;
            }
            return;
        }
        else if(chunkBufferPos == SMALLBUF_SIZE)
        {
            useChunkBuffer = true;
            chunkBufferPos = 0;
        }
        else
        {
            useChunkBuffer = false;
        }

        SurfaceView sv = (SurfaceView)findViewById(R.id.surfaceView);
        Canvas canvas = sv.getHolder().lockCanvas();

        // - take sample with size 2^n
        Complex[] data = new Complex[SMALLBUF_SIZE];
        Complex temp = new Complex(0, 0);
        double[] res = new double[data.length/2];
        int max_input = 0;
        double max_res = 0;
        int tmp_input = 0;

        // - multiply sample[i] with hannarray[i]
        for(int i = 0; i < data.length; i++)
        {
            if(useChunkBuffer)
            {
                tmp_input = Math.abs(chunkBuffer[i]);
                data[i] = new Complex(((double)chunkBuffer[i]) * HannWindow.window[i], 0);
            }
            else
            {
                tmp_input = Math.abs(audioData[i]);
                data[i] = new Complex(((double)audioData[i]) * HannWindow.window[i], 0);
            }

            if(tmp_input > max_input)
                max_input = tmp_input;
        }

        // - throw sample in fft
        data = FFT.fft(data);

        // - take result, throw second half away
        // - and throw it in complex.abs();
        displayDataBuffer[bufferPos] = new double[res.length];

        for(int i = 0; i < res.length; i++)
        {
            res[i] = Complex.abs(data[res.length + i]);
            // transfer result to display buffer
            displayDataBuffer[bufferPos][i] = res[i];
            if(max_res < res[i])
                max_res = res[i];
        }

        int x = 0, y = 0;
        double color = 0;
        double colorf = 0;
        if(canvas == null)
            return;

        // write bitmap data
        painter.setARGB(255, 255, 0, 0);
        x = bufferPos;
        //x = Math.round(((float) bufferPos / (float) (displayDataBuffer.length - 1)) * (display.getWidth() - 1));
        for(int i = 0; i < displayDataBuffer[bufferPos].length; i++)
        {
            y = Math.round(((float) i / (float) (displayDataBuffer[bufferPos].length - 1)) * (display.getHeight() - 1));
            colorf = ((displayDataBuffer[bufferPos][i] / max_res) * max_input) / 65536.0;
            colorf = 20.0 * Math.log10(colorf);
            colorf += 130.0;
            colorf = Math.max(colorf, 0.0);
            colorf *= (255.0/130.0);
  //          color = (int) displayDataBuffer[bufferPos][i];
            /*
            if(color > 0 && color < 200)
                color = Color.BLUE;
            else if(color >= 200 && color < 500)
                color = Color.GREEN;
            else
                color = Color.RED;
*/
            color = (colorf / 255.0);

            switch(display_color)
            {
                case 1: display.setPixel(x, y, Color.argb(255, (int)(color*255), (int)(Math.pow(color, 2)*255), (int)(Math.pow(color, 2)*255))); break;
                case 2: display.setPixel(x, y, Color.argb(255, (int)(Math.pow(color, 2)*255), (int)(color*255), (int)(Math.pow(color, 2)*255))); break;
                case 3: display.setPixel(x, y, Color.argb(255, (int)(Math.pow(color, 2)*255), (int)(Math.pow(color, 2)*255), (int)(color*255))); break;
                default: display.setPixel(x, y, Color.argb(255, (int)(color*255), (int)(color*255), (int)(color*255))); break;
            }
        }


        // draw result
        Rect src = new Rect(0, 0, display.getWidth(), display.getHeight());
        RectF dst = new RectF(0, 0, canvas.getWidth(), canvas.getHeight());

        // draw frequency axis
        canvas.drawBitmap(display, src, dst, painter);
        painter.setColor(Color.WHITE);
        canvas.drawLine(0, 0, 0, canvas.getHeight(), painter);
        canvas.drawLine(0, canvas.getHeight()-1, 5, canvas.getHeight()-1, painter);
        canvas.drawLine(0, (canvas.getHeight()/2)-1, 5, (canvas.getHeight()/2)-1, painter);
        canvas.drawLine(0, 0, 5, 0, painter);
        painter.setTextSize(10);
        canvas.drawText("0 Hz", 7, canvas.getHeight()-2, painter);
        canvas.drawText(Integer.toString((int)((float)RECORDER_SAMPLERATE/2.0f)) + " Hz", 7, 11, painter);
        canvas.drawText(Integer.toString((int)((float)RECORDER_SAMPLERATE/4.0f)) + " Hz", 7, (canvas.getHeight()/2)+6, painter);

        // draw amplitude axis
        //canvas.drawLine(canvas.getWidth()-1, 0, canvas.getWidth()-1, canvas.getHeight(), painter);
        canvas.drawRect(canvas.getWidth()-11, 0, canvas.getWidth()-1, canvas.getHeight()-1, painter);
        painter.setColor(Color.BLACK);
        canvas.drawRect(canvas.getWidth()-10, 1, canvas.getWidth()-2, canvas.getHeight()-2, painter);

        float delta_h = (float)canvas.getHeight() / 256.0f;
        float start_h = 1;
        for(int i = 0; i < 255; i++)
        {
            color = ((double)255-i)/255.0;
            switch(display_color)
            {
                case 1: painter.setColor(Color.argb(255, (int)(color*255), (int)(Math.pow(color, 2)*255), (int)(Math.pow(color, 2)*255))); break;
                case 2: painter.setColor(Color.argb(255, (int)(Math.pow(color, 2)*255), (int)(color*255), (int)(Math.pow(color, 2)*255))); break;
                case 3: painter.setColor(Color.argb(255, (int)(Math.pow(color, 2)*255), (int)(Math.pow(color, 2)*255), (int)(color*255))); break;
                default: painter.setColor(Color.argb(255, (int)(color*255), (int)(color*255), (int)(color*255))); break;
            }
            canvas.drawRect(canvas.getWidth()-10, start_h + ((i+1)*delta_h), canvas.getWidth()-2, Math.min(start_h + (i*delta_h), canvas.getHeight()-2), painter);
        }
        painter.setColor(Color.WHITE);
        canvas.drawLine(canvas.getWidth()-11, canvas.getHeight()-1, canvas.getWidth(), canvas.getHeight()-1, painter);
        canvas.drawText("0 dBm", canvas.getWidth()-45, 11, painter);
        canvas.drawText("-130 dBm", canvas.getWidth()-60, canvas.getHeight()-2, painter);

        sv.getHolder().unlockCanvasAndPost(canvas);


        if(bufferPos < displayDataBuffer.length - 1)
            bufferPos++;
        else
            bufferPos = 0;
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
//    public Action getIndexApiAction() {
//        Thing object = new Thing.Builder()
//                .setName("Main Page") // TODO: Define a title for the content shown.
//                // TODO: Make sure this auto-generated URL is correct.
//                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
//                .build();
//        return new Action.Builder(Action.TYPE_VIEW)
//                .setObject(object)
//                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
//                .build();
//    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client.connect();
//        AppIndex.AppIndexApi.start(client, getIndexApiAction());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        display_color = Integer.parseInt(prefs.getString("list_preference_color", "3"));
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        AppIndex.AppIndexApi.end(client, getIndexApiAction());
//        client.disconnect();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        display_color = Integer.parseInt(prefs.getString("list_preference_color", "3"));
    }

    @Override
    public void onResume()
    {
        super.onResume();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        display_color = Integer.parseInt(prefs.getString("list_preference_color", "3"));
    }
}