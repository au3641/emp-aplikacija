package spektrogram.emp.spektrogram_emp;

import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;

public class GetFilenameHelpActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_filename_help);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setSubtitle(R.string.get_filename_subtitle);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = new Intent(this, MainActivity.class);
                NavUtils.navigateUpTo(this, upIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void OnCancelClicked(View view) {
        Intent returnIntent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("spektrogram.emp.returned_filename", "");
        returnIntent.putExtras(bundle);
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }

    public void OnSelectFilenameClicked(View view) {
        String filename = ((EditText)findViewById(R.id.filenameEditText)).getText().toString();
        Intent returnIntent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("spektrogram.emp.returned_filename", filename);
        returnIntent.putExtras(bundle);
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}
