package spektrogram.emp.spektrogram_emp;

import android.content.Intent;
import android.database.MatrixCursor;
import android.media.MediaPlayer;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class SavedFilesActivity extends AppCompatActivity {

    public static final String FIRST_COLUMN = "First";
    public static final String SECOND_COLUMN = "Second";
    public static final String THIRD_COLUMN = "Third";

    private Toolbar mToolbar;
    private ArrayList<HashMap<String, String>> mList;
    private int mSelectedIndex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_files);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setSubtitle(R.string.saved_files_subtitle);

        initListView();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = new Intent(this, MainActivity.class);
                NavUtils.navigateUpTo(this, upIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initListView()
    {
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Lib");
        File[] fileList = dir.listFiles();
        mList = new ArrayList<>();
        for (int i = 0; i < fileList.length; i++) {
            HashMap<String,String> t1 = new HashMap<>();
            t1.put(FIRST_COLUMN, fileList[i].getName());
            try {
                MediaPlayer player = new MediaPlayer();
                player.setDataSource(fileList[i].getPath());
                player.prepare();
                int duration = player.getDuration();
                String durationString = String.format("%.2fs", (float)duration/1000);
                t1.put(SECOND_COLUMN, durationString);
                player.release();

            }catch (Exception e){

            }
            t1.put(THIRD_COLUMN, getSizeString(fileList[i].length()));
            mList.add(t1);
        }

        ListView filesListView = (ListView) findViewById(R.id.filesListView);
        ListViewAdapter adapter = new ListViewAdapter(this, mList);
        filesListView.setAdapter(adapter);
        findViewById(R.id.buttonDelete).setEnabled(false);
        findViewById(R.id.buttonOpen).setEnabled(false);
        filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSelectedIndex = position;
                findViewById(R.id.buttonDelete).setEnabled(true);
                findViewById(R.id.buttonOpen).setEnabled(true);
            }
        });
    }

    private String getSizeString(long bytes){
        long kb = bytes/1024;
        if(kb > 1024){
            long mb = kb/1024;
            return mb+"MB";
        }
        return kb+"KB";
    }

    public void OnDeleteClicked(View view) {
        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Lib/" + mList.get(mSelectedIndex).get(FIRST_COLUMN));
        f.delete();
        initListView();
    }

    public void OnOpenClicked(View view) {
        String filename = Environment.getExternalStorageDirectory().getAbsolutePath()+"/Lib/" + mList.get(mSelectedIndex).get(FIRST_COLUMN);
        Intent intent = new Intent(SavedFilesActivity.this, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("spektrogram.emp.filename", filename);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
