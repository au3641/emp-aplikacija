# README #

Andrej Ulčar 63130266  
Blaž Berglez 63080446

###Opis aplikacije###

Za nalogo želiva izdelati aplikacijo, ki nam omogoča izrisovanje spektrograma iz podatkov mikrofona, v realnem času. Aplikacija bo imela sposobnostjo hranjenja zvočnih posnetkov in analiziranja že obstoječih, prav tako pa tudi možnost spreminjanja nastavitev izrisa. Celotna podoba aplikacija bo sledila 'material design' pravilom.

#####**Glavni zaslon**#####

To bo prvi zaslon/aktivnost, ki jo uporabnik vidi, ko odpre aplikacijo. Tu se bo izrisoval naš spektrogram, prisotni pa bodo tudi gumbi za hitre akcije in za dostop do drugih aktivnosti:

+ start/stop
+ barva
+ frekvenca
+ nastavitve
+ datoteke

Spektrogram se bo na začetku izrisoval iz podatkov, ki jih bo zajel z mikrofonom. Uporabnik bo lahko delovanje in izgled spektrograma spremenil, ter spremenil tudi vir podatkov na izbrano zvočno datoteko.

#####**Nastavitve**#####

V tej aktivnosti bo uporabnik lahko spremenil delovanje spektrograma. Možno bo spremeniti resolucijo zajema, glajenje vhodnih podatkov, barvo izrisa, ter še kaj. Nekaj nastavitev bo lahko uporabnik spremenil tudi iz glavnega zaslona preko hitrih akcij, npr. barvo.

#####**Datoteke**#####

Ta aktivnost bo prikazovala zvočne posnetke, ki smo jih zajeli v glavnem zaslonu. Te bo moč naložiti v glavni zaslon, kjer si bomo lahko ponovno ogledali spektrogram posnetka. Prisotni pa bodo tudi osnovni podatki o posnetku, kot so: ime, trajanje, velikost.